package me.calebbassham.languages

import me.calebbassham.languages.message.Message
import me.calebbassham.languages.parser.Parser

private fun replaceOptions(options: Map<String, String>, message: Message): Message {
    var m = message.message

    for ((key, value) in options) {
        m = m.replace("{@$key}", value)
    }

    return Message(message.messageKey, m, message.messageType)
}

private fun replaceOptions(options: Map<String, String>, messages: HashMap<String, Message>): HashMap<String, Message> {
    for ((key, value) in messages) {
        messages[key] = replaceOptions(options, value)
    }

    return messages
}

private fun transformMessages(messages: HashMap<String, Message>, messageTransformer: (message: Message) -> Message): HashMap<String, Message> {
    for ((key, value) in messages) {
        messages[key] = messageTransformer(value)
    }

    return messages
}

fun loadLanguage(input: String, parser: Parser, messageTransformer: ((message: Message) -> Message)? = null): HashMap<String, Message> {
    val options = parser.getOptions(input)
    val messages = replaceOptions(options, parser.getMessages(input))

    if (messageTransformer != null) {
        return transformMessages(messages, messageTransformer)
    }

    return messages
}


