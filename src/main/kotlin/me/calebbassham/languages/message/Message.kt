package me.calebbassham.languages.message

data class Message(val messageKey: String, val message: String, val messageType: MessageType) {

    fun replaceVariable(variableName: String, value: String) = Message(messageKey, message.replace("{{ $variableName }}", value), messageType)

}