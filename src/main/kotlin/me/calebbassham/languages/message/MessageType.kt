package me.calebbassham.languages.message

enum class MessageType {
    PLAIN, JSON
}