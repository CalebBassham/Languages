package me.calebbassham.languages.parser

import me.calebbassham.languages.message.Message
import me.calebbassham.languages.message.MessageType
import org.json.JSONException
import org.json.JSONObject

class JsonLanguageFileParser : Parser {

    override fun getOptions(input: String): HashMap<String, String> {
        val optionsJSONObject = JSONObject(input).getJSONObject("options")

        val options = HashMap<String, String>()
        for (option in optionsJSONObject.keySet()) {
            options[option] = optionsJSONObject[option].toString()
        }

        return options
    }

    override fun getMessages(input: String): HashMap<String, Message> {
        val messagesJsonObject = JSONObject(input).getJSONObject("messages")

        val messages = HashMap<String, Message>()
        for (key in messagesJsonObject.keySet()) {

            val messageType: MessageType = try {
                messagesJsonObject.getJSONArray(key)
                MessageType.JSON
            } catch (e: JSONException) {
                MessageType.PLAIN
            }

            messages[key] = Message(key, messagesJsonObject[key].toString(), messageType)
        }

        return messages
    }
}