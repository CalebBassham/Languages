package me.calebbassham.languages.parser

import me.calebbassham.languages.message.Message

interface Parser {

    fun getOptions(input: String): HashMap<String, String>

    /**
     * Get messages and replace
     */
    fun getMessages(input: String): HashMap<String, Message>

}